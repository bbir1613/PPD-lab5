﻿using PPD_Llab5.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPD_Llab5
{
    public partial class Form1 : Form,Observable
    {
        private Controller.Controller controller;
        private Cumparator cumparator;
        public Form1()
        {
            InitializeComponent();
            controller = new Controller.Controller(this);
            cumparator = new Cumparator(controller);
            cumparator.buy();
            GetProductsAsync();
        }

        async void GetProductsAsync()
        {
            List<Produs> products = await Task.Run(() => controller.Products);
            listBox1.Items.Clear();
            listBox1.Items.AddRange(products.ToArray());
        }

        async private void addToCartButton_Click(object sender, EventArgs e)
        {
            Produs product = (Produs)listBox1.SelectedItem;
            if(product != null)
            {
                List<Produs> products = await Task.Run(() =>controller.AddToCart(product));
                GetProductsAsync();
                listBox2.Items.Clear();
                listBox2.Items.AddRange(products.ToArray());
            }
        }

        async private void buyButton_Click(object sender, EventArgs e)
        {
            await Task.Run(() => controller.Buy());
            List<Produs> products = controller.CartProducts;
            listBox2.Items.Clear();
            listBox2.Items.AddRange(products.ToArray());

        }

        public void updateStock()
        {
            this.BeginInvoke(new MethodInvoker(()=> GetProductsAsync()));
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            cumparator.stop();
            Application.Exit();
        }
    }
}
