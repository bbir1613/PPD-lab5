﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPD_Llab5.Domain
{
    public class Inventar
    {
        Produs[] initial;
        double sum;
        
        public Inventar(Produs[] products)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\Users\\Bogdan\\Desktop\\inventar.txt");
            file.Close();
            initial = products;
            sum = 0;
        }

        public void calculate(Produs[] products, Factura[] facturi,double sum)
        {
            double sell = sum - this.sum;
            var soldProducts = new ConcurrentDictionary<int, Produs>(Environment.ProcessorCount * 2, 3);
            foreach (Factura f in facturi)
            {
                sell = sell - f.Sum;
                foreach(Produs p in f.Products)
                {
                    soldProducts.AddOrUpdate(p.Cod, new Produs(p.Nume, p.Cod, p.Pret_unitar, p.Unit_masura), (key, oldvalue) =>
                     {
                         oldvalue.Unit_masura = oldvalue.Unit_masura + p.Unit_masura;
                         return oldvalue;
                     });
                }
            }

            if (sell != 0)
                {
                    Console.WriteLine("SOMETHING WENT WRONG" + sell.ToString());
                }else
                {
                Console.WriteLine("INVETAR INCHEIAT CU SUCCES");
                StreamWriter file = File.AppendText("C:\\Users\\Bogdan\\Desktop\\inventar.txt");
                file.WriteLine(' ');
                file.WriteLine("Suma incasata : " + (sum- this.sum).ToString());
                file.WriteLine("Produse : ");
                foreach(Produs p in soldProducts.Values)
                {
                    file.Write(p.ToString()+"  ,  ");
                }
                file.WriteLine(' ');
                file.Flush();
                file.WriteLine("\nFacturi : ");
                foreach(Factura f in facturi)
                {
                    file.Write(f.ToString() + "  ,  ");
                }
                file.Flush();
                file.Close();
            }
                this.sum = sum;
        }
    }
}
