﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPD_Llab5.Domain
{
    public interface Observable
    {
        void updateStock();
    }
}
