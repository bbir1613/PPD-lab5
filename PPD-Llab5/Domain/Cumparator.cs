﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PPD_Llab5.Controller;
using System.Threading;

namespace PPD_Llab5.Domain
{
    public class Cumparator
    {
        private Controller.Controller controller;
        private bool buying;
        private int frequency;

        public Cumparator(Controller.Controller controller)
        {
            this.controller = controller;
            this.buying = true;
            this.frequency = 500;
        }

        public void buy()
        {
            Task.Run(() =>
            {
                Random random = new Random();
                while (buying)
                {
                    Thread.Sleep(this.frequency);
                    if (buying) {
                        List<Produs> products = controller.Products;
                        int index = random.Next(0, products.Count);
                        Produs p = products[index];
                        controller.Buy(new List<Produs>() { new Produs(p.Nume, p.Cod, p.Pret_unitar, random.Next(1,5))});
                    }
                }
            });
        }

        public void stop()
        {
            buying = false;
        }
    }
}
