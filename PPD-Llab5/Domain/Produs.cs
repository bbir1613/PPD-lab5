﻿
namespace PPD_Llab5.Domain
{
    public class Produs
    {
        public Produs(string name, int code, double price, int quantity)
        {
            Nume = name;
            Cod = code;
            Pret_unitar = price;
            Unit_masura = quantity;
        }

        public Produs()
        {
        }

        public string Nume { set; get; }
        public int Cod { set; get; }
        public double Pret_unitar { set; get; }
        public int Unit_masura { set; get; }

        public override string ToString()
        {
            return Cod.ToString()+" " + Nume.ToString()+"  "+ Pret_unitar.ToString()+" " + Unit_masura.ToString();
        }
    }
}
