﻿using System;
using System.Collections.Generic;


namespace PPD_Llab5.Domain
{
    public class Factura
    {
        public Factura(DateTime dateTime,List<Produs> products)
        {
            Data = dateTime;
            Products = products;
            Sum = 0.0;
            foreach(var p in products)
            {
                Sum = Sum + (p.Pret_unitar*p.Unit_masura);
            }
        }
        public DateTime Data { set; get; }
        public List<Produs> Products { set; get; }
        public double Sum { set; get; }

        public override string ToString()
        {
            return " Sum : " + Sum.ToString() +" nr of products : " + Products.Count.ToString();
        }
    }
}
