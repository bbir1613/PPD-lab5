﻿using PPD_Llab5.Domain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PPD_Llab5.Controller
{
    public class Controller
    {
        ConcurrentDictionary<int, Produs> products;
        ConcurrentDictionary<int, Produs> cartProducts;
        ConcurrentBag<Factura> facturi; 
        Observable obs;
        Double sum;
        private Object thisLock = new Object();

        public Controller(Observable obs)
        {
            this.obs = obs;
            var ps = new List<Produs>() { new Produs("p1", 1, 10, 100000), new Produs("p2", 2, 1, 100000), new Produs("p3", 3, 100, 100000) };
            products = new ConcurrentDictionary<int, Produs>(Environment.ProcessorCount * 2, ps.Count);
            foreach(var p in ps)
            {
                products.TryAdd(p.Cod, p);
            }
            cartProducts = new ConcurrentDictionary<int, Produs>(Environment.ProcessorCount * 2, ps.Count);
            facturi = new ConcurrentBag<Factura>();
            sum = 0;
            verify();
        }

        public List<Produs> Products{ get {
                List<Produs> products = new List<Produs>();
                foreach(var p in this.products.Values.ToList())
                {
                    Produs product;
                    Produs addProduct = new Produs();
                    addProduct.Cod = p.Cod;
                    addProduct.Nume = p.Nume;
                    addProduct.Pret_unitar = p.Pret_unitar;
                    addProduct.Unit_masura = p.Unit_masura;
                    this.cartProducts.TryGetValue(p.Cod, out product);
                    if (product !=null)
                    {
                        addProduct.Unit_masura = addProduct.Unit_masura - product.Unit_masura;
                    }
                    products.Add(addProduct);
                }
                return products;
            }
        }

        public void verify()
        {
            Produs[] p = new Produs[products.Count];
            products.Values.CopyTo(p, 0);
            Inventar invetar = new Inventar(p);
            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(7000);
                    Factura[] f;//= new Factura[facturi.Count];
                    p = new Produs[products.Count];
                    double s = 0;
                    lock (this.thisLock) {
                        products.Values.CopyTo(p, 0);
                        f = new Factura[facturi.Count];
                        facturi.CopyTo(f, 0);
                        s = s+sum;
                        facturi = new ConcurrentBag<Factura>();
                    }
                    invetar.calculate(p,f,s);
                }
            });
        }

        public List<Produs> CartProducts { get { return cartProducts.Values.Select(p => p).ToList(); }
        }
        public void Buy(List<Produs> products)
        {
            lock (thisLock) { 
                Factura factura = new Factura(new DateTime(), products);
                facturi.Add(factura);
                foreach (var p in products)
                {
                    this.products.AddOrUpdate(p.Cod, new Produs(), (key, oldvalue) =>
                     {
                         oldvalue.Unit_masura = oldvalue.Unit_masura - p.Unit_masura;
                         return oldvalue;
                     });
                    sum = sum + (p.Unit_masura * p.Pret_unitar);
                }
            }
            obs.updateStock();
        }

        public void Buy()
        {
            Buy(CartProducts);
            cartProducts = new ConcurrentDictionary<int, Produs>(Environment.ProcessorCount * 2, this.products.Values.Count);
        }

        public List<Produs> AddToCart(Produs product)
        {
            cartProducts.AddOrUpdate(product.Cod, new Produs(product.Nume, product.Cod, product.Pret_unitar, 1), (key, oldValue) =>
            {
                oldValue.Unit_masura = oldValue.Unit_masura + 1;
                return oldValue;
            });

            return CartProducts;
        }
    }
}
